import React, { Component } from 'react';
import Router from 'next/router';

const withAuth = (Page) => {
	return class Auth extends Component {
		static async getInitialProps(ctx) {
      const isServer = !!ctx && !!ctx.req;

      if(Page.getInitialProps) {
        return await Page.getInitialProps(ctx);
      }

      if(isServer) {
        if(!ctx.req.user) {
          if(ctx.res) {
            ctx.res.redirect('/login');
          }
        }
      } else {
        Router.push('/login');
      }
    }

    render() {
      if(this.props.user) {
        return <Page {...this.props}/>
      }

      Router.push('/login', null, { shallow: true });
      
      return <div>Not authenticated</div>;
    }
	}
}

export default withAuth;
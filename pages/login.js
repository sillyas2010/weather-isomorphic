import React, { Component } from 'react';
import Link from 'next/link';
import Router from 'next/router';
import { StyleSheet, css } from 'aphrodite';

import SignInButton from '../components/SignInButton';

export default class Login extends Component {
  static async getInitialProps(ctx) {
    const isServer = !!ctx && !!ctx.req;

    if(isServer) {
      if(ctx.req.user) {
        if(ctx.res) {
          ctx.res.redirect('/');
        }
      }
    } else {
      Router.push('/');
    }
  }

  providers = [
    {
      title: 'Google',
      color: '#4185f4',
      icon: '/google-logo.svg',
      url: '/auth/google'
    }
  ]

  render() {
    if(this.props.user) {
      Router.push('/', null, { shallow: true });
    }

    return (
      <div className={css(cx.main)}>
        <div className={css(cx.buttonsList)}>
          {
            this.providers.map((provider, ind) => (
              <SignInButton key={`${provider.title}-${ind}`} {...provider}/>
            ))
          }
        </div>
      </div>
    )
  }
}

const cx = StyleSheet.create({
  main: {
    height: '100%'
  },
  buttonsList: {
    display: 'flex',
    height: '100%',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center'
  }
});
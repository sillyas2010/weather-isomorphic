import React, { Component } from 'react';
import { AreaChart, Area, XAxis, YAxis, Tooltip, CartesianGrid, ResponsiveContainer } from 'recharts';
import { toast } from 'react-toastify';
import config from '../server/config';
import withAuth from '../hoc/withAuth';

class Index extends Component {
  state = {
    weatherData: []
  }

  componentDidMount() {
    fetch(config.host + '/api/get-weather', { credentials: 'same-origin' }).then((res) => {
      return res.json();
    }).then((json) => {
      if(json.data) {
        const weatherData = json.data.reduce((resObj, timeEl) => {
          if(timeEl && timeEl.cities && timeEl.cities.length) {
            timeEl.cities.forEach((cityObj) => {
              const currentCity = resObj[cityObj.city],
                currentWeather = { 
                  title: cityObj.city,
                  temperature: cityObj.temperature, 
                  date: timeEl.createdAt 
                };

              if(currentCity && currentCity.length) {
                resObj[cityObj.city] = [ ...currentCity, currentWeather ];
              } else {
                resObj[cityObj.city] = [currentWeather];
              }
            })
          }

          return resObj;
        }, {})

        toast.success('Successfully fetched weather');

        this.setState({ weatherData });
      }
    }).catch((err) => {
      toast(err);
    });
  }
  
  render() {
    const { weatherData } = this.state;

    return (
      <div className="main">
        {
          Object.keys(weatherData).map((cityName, ind) => (
            <div key={`${cityName}-${ind}`}>
              <h3>{cityName}</h3>
              <ResponsiveContainer height={200}>
                <AreaChart
                  data={weatherData[cityName]}
                  margin={{
                    top: 10, right: 30, left: 0, bottom: 0,
                  }}
                >
                  <CartesianGrid strokeDasharray="3 3" />
                  <XAxis dataKey="date" />
                  <YAxis />
                  <Tooltip />
                  <Area type="monotone" dataKey="temperature" stroke="#8884d8" fill="#8884d8" />
                </AreaChart>
              </ResponsiveContainer>
            </div>
          ))
        }
      </div>
    )
  }
}

export default withAuth(Index);
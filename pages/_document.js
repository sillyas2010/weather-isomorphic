import Document, { Html, Head, Main, NextScript } from 'next/document';
import { StyleSheetServer } from 'aphrodite';

class DefaultDoc extends Document {
  static async getInitialProps(ctx) {
    const { html, css } = StyleSheetServer.renderStatic(() => ctx.renderPage()),
      ids = css.renderedClassNames;

    return { ...html, css, ids }
  }

  constructor(props) {
    super(props)
    /* Take the renderedClassNames from aphrodite (as generated
    in getInitialProps) and assign them to __NEXT_DATA__ so that they
    are accessible to the client for rehydration. */
    const { __NEXT_DATA__, ids } = props
    if (ids) {
      __NEXT_DATA__.ids = this.props.ids
    }
  }

  render() {
    return (
      <Html>
        <Head>
          <style
            data-aphrodite
            dangerouslySetInnerHTML={{ __html: this.props.css.content }}
          />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}

export default DefaultDoc
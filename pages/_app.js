import React, { Component } from 'react';
import App from 'next/app';
import Link from 'next/link';
import Router from 'next/router';
import { Layout, Menu, Icon } from 'antd';
import { ToastContainer, toast } from 'react-toastify';
import fetch from 'isomorphic-unfetch';
import 'react-toastify/dist/ReactToastify.css';
import { StyleSheet, css } from 'aphrodite';

const { Header, Footer, Sider, Content } = Layout;

if (typeof window !== 'undefined') {
  /* StyleSheet.rehydrate takes an array of rendered classnames,
  and ensures that the client side render doesn't generate
  duplicate style definitions in the <style data-aphrodite> tag */
  StyleSheet.rehydrate(window.__NEXT_DATA__.ids)
}

export default class Page extends App {
  static async getInitialProps({ ctx, Component }) {
    const { req } = ctx,
      isServer = !!req;

    let user = null,
      pageProps = {};

    if(isServer) {
      if(req && req.user) {
        const { 
          googleId,
          email,
          firstName,
          lastName,
        } = req.user;

        user = {
          googleId,
          email,
          firstName,
          lastName,
        };
      }
    } else {
      const res = await fetch('/auth/user'),
        json = await res.json();

      if(json && json.user) {
        user = json.user;
      }
    }

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }
    
    return {  
      pageProps,  
      user
    }
  }

  state = {
    collapsed: true
  }

  appAuthor = 'Illia Hloza'

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    })
  }
  
  logOut = async () => {
    const res = await fetch('/auth/logout'),
      json = await res.json();

    if (!res.ok) {
      toast.error(JSON.stringify(json));
    }
    
    Router.reload();
  }

  render() {
    const { Component, pageProps, user } = this.props,
      { collapsed } = this.state,
      initials = user && user.firstName && user.lastName ? 
        `${user.firstName[0]}${user.lastName[0]}` 
        : 
        `FL`;

    return(
      <div className={css(cx.pageWrapper)}>
        <Layout className={"ant-layout-has-sider container " + css(cx.siderLayout)}>
          <Sider trigger={null} collapsible collapsed={collapsed}>
            <div className={css(cx.appLogoWrapper)}>
              <span className={collapsed ? css(cx.appTitle) : css(cx.appTitle, cx.appTitleVisible)}>Weather</span>
            </div>
            <Menu theme="dark" mode="inline">
              <Menu.Item key="dashboard">
                <div className="link-item">
                  <a href="/">
                    <Icon type="dashboard" />
                    <span>Dashboard</span>
                  </a>
                </div>
              </Menu.Item>
              <Menu.Item key="profile">
                <div className="link-item">
                  <a href="/profile">
                    <Icon type="user" />
                    <span>Profile</span>
                  </a>
                </div>
              </Menu.Item>
            </Menu>
          </Sider>
        </Layout>
        <Layout>
          <Header className={css(cx.header)}>
            <Icon className="trigger" type={collapsed ? 'menu-unfold' : 'menu-fold'} onClick={this.toggle} />

            { !!user ?
              <div className={css(cx.accountSection)}>
                <a href="/profile">
                  <div className={css(cx.accountCircle)}>
                    {initials}
                  </div>
                </a>
                <form action={"/auth/logout"} name="logform" className={css(cx.logOutForm)}>
                  <Icon type='logout' onClick={this.logOut.bind(this)} />
                </form>
              </div>
              :
              <div className={css(cx.accountSection)}>
                <Link href="/login">
                  <a>
                    Login
                  </a>
                </Link>
              </div>
            }
          </Header>
          <Content className={css(cx.contentSection)}>
            <Component {...pageProps} user={user} />
          </Content>
          <Footer className={css(cx.footerSection)}>{this.appAuthor} ©</Footer>
        </Layout>
        <ToastContainer/>
      </div>
    )
  }
}

const cx = StyleSheet.create({
  appLogoWrapper: {
    marginTop: '.3rem',
    textAlign: 'center',
    lineHeight: '3rem'
  },
  appTitle: {
    fontSize: '1.5em',
    textTransform: 'uppercase',
    color: '#fff',
    opacity: 0,
    transition: 'opacity .3s'
  },
  appTitleVisible: {
    opacity: 1
  },
  pageWrapper: {
    display: 'flex',
    minHeight: '100vh',
    maxWidth: '100vw',
    overflow: 'hidden'
  },
  siderLayout: { 
    flex: '0 0 auto' 
  },
  header: { 
    background: '#fff',
    padding: '0 16px' 
  },
  accountSection: { 
    float: 'right',
    display: 'flex', 
    alignItems: 'center' 
  },
  accountCircle: { 
    width: 50, 
    height: 50, 
    lineHeight: '50px', 
    textAlign: 'center', 
    fontSize: 16, 
    color: '#FFFFFF', 
    float: 'left', 
    borderRadius: '50%', 
    background: 'silver'
  },
  logOutForm: { 
    display: 'inline-block',
    marginLeft: 16
  },
  contentSection: { 
    padding: 16,
    minHeight: 280 
  },
  footerSection: { 
    textAlign: 'center' 
  }
});
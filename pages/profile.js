import React, { Component } from 'react';

import withAuth from '../hoc/withAuth';

class Profile extends Component {
  render() {
    const { user } = this.props;

    return (
      <div>
        {
          Object.keys(user).map((key, ind) => (
            <div key={`${key}-${ind}`} style={{ marginBottom: 15 }}>
              <h5>{key}:</h5><span>{user[key]}</span> 
            </div>
          ))
        }
      </div>
    )
  }
}

export default withAuth(Profile);
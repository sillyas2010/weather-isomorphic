import React, { Component } from 'react';
import Link from 'next/link';
import { StyleSheet, css } from 'aphrodite';

export default function SignInButton({ icon, color, title, url }) {
  let stl = cx({ color, icon });

  return (
    <Link href={url}>
      <a>
        <div className={css(stl.buttonWrapper)}>
          <div className={css(stl.buttonIcon)}></div>
          <div className={css(stl.buttonText)}>
            Sign in with {title}
          </div>
        </div>
      </a>
    </Link>
  )
};

const cx = ({ color, icon }) => (
  StyleSheet.create({
    buttonWrapper: {
      display: 'flex',
      backgroundColor: color,
      borderRadius: '5px',
      padding: 1
    },
    buttonIcon: {
      padding: '1.5rem',
      borderRadius: '5px',
      background: `url(${icon}) #fff center/20px no-repeat`
    },
    buttonText: {
      padding: '10px 15px',
      lineHeight: '1.75rem',
      fontSize: '1rem',
      color: '#fff'
    }
  })
);
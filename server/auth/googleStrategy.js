const GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
const User = require('../models/User');

const strategy = new GoogleStrategy(
    {
        clientID: process.env.GOOGLE_CLIENT_ID,
        clientSecret: process.env.GOOGLE_CLIENT_SECRET,
        callbackURL: '/auth/google/callback'
    },
    (token, tokenSecret, profile, done) => {
        let { id, name, photos, emails } = profile;
        emails = emails.map(emailObj => emailObj.value);

        User.findOne({ 'googleId': id }, async (err, userMatch) => {
            // handle errors here:
            if (err) {
                console.log('Error!! trying to find user with googleId');
                console.log(err);
                return done(null, false);
            }
            // if there is already someone with that googleId
            if (userMatch) {
                if(!userMatch.google || !userMatch.google.googleId) {
                    userMatch.google = {};
                    userMatch.google.googleId = id;

                    let savedUser = await userMatch.save();

                    return done(null, savedUser);
                }
                return done(null, userMatch);
            } else {
                // if no user in our db, create a new user with that googleId
                const newGoogleUser = new User({
                    googleId: id,
                    firstName: name.givenName,
                    lastName: name.familyName,
                    email: emails[0]
                });
                // save this user
                newGoogleUser.save((err, savedUser) => {
                    if (err) {
                        console.log('Error!! saving the new google user');
                        console.log(err);
                        return done(null, false)
                    } else {
                        return done(null, savedUser)
                    }
                })
            }
        })
    }
);

module.exports = strategy;
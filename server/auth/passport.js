const passport = require('passport');
const GoogleStrategy = require('./googleStrategy');
const User = require('../models/User');

passport.serializeUser((user, done) => {
    done(null, { _id: user._id })
});

passport.deserializeUser((id, done) => {
    User.findOne(
        { _id: id },
        {
            'googleId': 1,
            'isAdmin': 1,
            'firstName': 1,
            'lastName': 1,
            'email': 1
        },
        (err, user) => {
            done(null, user)
        }
    )
});

// ==== Register Strategies ====
passport.use(GoogleStrategy);

module.exports = passport;
const express = require('express');
const router = express.Router();
const User = require('../models/User');
const passport = require('./passport');

router.get('/google', passport.authenticate('google', { scope: ['profile', 'email'] }));
router.get(
    '/google/callback',
    passport.authenticate('google', {
        successRedirect: `/`,
        failureRedirect: `/login?message-id=google-login-failed`
    })
);

// this route is just used to get the user basic info
router.get('/user', (req, res, next) => {
    if (req.user) {
        return res.json({ user: req.user })
    } else {
        return res.json({ user: null })
    }
});

const logOutFunc = (req, res) => {
  if (req.user) {
      req.session.destroy();
      res.clearCookie('connect.sid'); // clean up!
      return res.json(204);
  } else {
      return res.json(400, { err: "You cannot logout without sign-in" });
  }
};

router.post('/logout', logOutFunc);

router.get('/logout', logOutFunc)

module.exports = router;
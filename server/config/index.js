const environments = {
  current: process.env.NODE_ENV === 'production' ? 'prod' : 'dev',
  dev: {
      host: 'http://localhost:3000',
      dbName: process.env.DB_NAME,
      dbHost: process.env.DB_HOST,
      dbUsername: process.env.DB_USER,
      dbPassword: process.env.DB_PASS
  },
  prod: {
      host: 'https://weather-isomorphic.herokuapp.com',
      dbName: process.env.DB_NAME,
      dbHost: process.env.DB_HOST,
      dbUsername: process.env.DB_USER,
      dbPassword: process.env.DB_PASS
  }
},
currentConfig = environments[environments.current];

module.exports = {
    ...currentConfig,
    environment: environments.current,
    whitelistCORS : [currentConfig.host],
    port: process.env.PORT || 3000,
    passPhrase: process.env.PASS_PHRASE || 'this is the default passphrase',
    weatherApiKey: process.env.WEATHER_API_KEY,
    weatherApiTemplate: 'http://api.weatherstack.com/current?access_key=${apiKey}&query=${city}',
    weatherCities: ['Kiev', 'Lviv', 'Kharkiv'],
    // mongodb connection route
    dbURI : `mongodb+srv://${currentConfig.dbUsername}:${currentConfig.dbPassword}@${currentConfig.dbHost}/${currentConfig.dbName}?retryWrites=true&w=majority`,
};


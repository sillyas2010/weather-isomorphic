require('dotenv').config();

const mongoose = require("mongoose"),
	express = require("express"),
	bodyParser = require("body-parser"),
	passport = require('passport'),
	session = require('express-session'),
	MongoStore = require('connect-mongo')(session),
	next = require('next'),
	path = require('path'),
	url = require('url'),
	cluster = require('cluster'),
	numCPUs = require('os').cpus().length,
	config = require('./config'),
	api = require('./api'),
  auth = require('./auth'),
  { startCron } = require('./cron');

const dev = config.environment === 'dev',
	port = config.port;

// connects our back end code with the database
mongoose.connect(
	config.dbURI,
	{useNewUrlParser: true, useUnifiedTopology: true}
);

let db = mongoose.connection;

db.once("open", () => console.log("connected to the database"));

// checks if connection with the database is successful
db.on("error", console.error.bind(console, "MongoDB connection error:"));

// Multi-process to utilize all CPU cores.
if (!dev && cluster.isMaster) {
  console.log(`Node cluster master ${process.pid} is running`);

  // Fork workers.
  for (let i = 0; i < numCPUs; i++) {
    cluster.fork();
  }

  cluster.on('exit', (worker, code, signal) => {
    console.error(`Node cluster worker ${worker.process.pid} exited: code ${code}, signal ${signal}`);
  });

} else {
  const nextApp = next({ dir: '.', dev });
  const nextHandler = nextApp.getRequestHandler();

  nextApp.prepare().then(() => {
		const server = express();

		// (optional)
		// bodyParser parses the request body to be a readable json format
		server.use(bodyParser.json());
		server.use(bodyParser.urlencoded({extended: false}));

		server.use(
			session({
				secret: config.passPhrase,
				store: new MongoStore({ mongooseConnection: db }),
				resave: false,
				saveUninitialized: false
			})
		);

		// ===== Passport ====
		server.use(passport.initialize());
		server.use(passport.session()); // will call the deserializeUser

		server.use((req, res, next) => {
			res.header("Access-Control-Allow-Origin", "*");
			res.header("Access-Control-Allow-Credentials", "true");
			res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
			next();
		});

		if (!dev) {
			// Enforce SSL & HTTPS in production
			server.use((req, res, next) => {
				var proto = req.headers["x-forwarded-proto"];
				if (proto === "https") {
					res.set({
						'Strict-Transport-Security': 'max-age=31557600' // one-year
					});
					return next();
				}
				res.redirect("https://" + req.headers.host + req.url);
			});
		}
		
		// Static files
		// https://github.com/zeit/next.js/tree/4.2.3#user-content-static-file-serving-eg-images
		server.use('/static', express.static(path.join(__dirname, 'static'), {
			maxAge: dev ? '0' : '365d'
		}));			

		server.use('/auth', auth);

		server.use("/api", api);

		// Default catch-all renders Next app
		server.get('*', (req, res) => {
			const parsedUrl = url.parse(req.url, true);
			nextHandler(req, res, parsedUrl);
    });
    
    startCron();

		server.listen(port, (err) => {
			if (err) throw err;
			console.log(`Listening on http://localhost:${port}`);
		});
	});
}
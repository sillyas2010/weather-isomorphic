const express = require('express'),
  router = express.Router(),
  cors = require("cors"),
  config = require('../config'),
  Weather = require('../models/Weather'),
  { collectWeather } = require('../func');

const whitelist = config.whitelistCORS,
  corsOptions = {
    origin: (origin, callback) => {
      if (!origin || whitelist.indexOf(origin) !== -1) {
        callback(null, true)
      } else {
        callback(new Error('Not allowed by CORS'))
      }
    }
  };

//Accept CORS from whitelisted domains
router.use(cors(corsOptions));

router.use((req, res, next) => {
  if (req.user) {
    return next();
  }

  return res.json(401, { success: false, error: 'Access Forbidden' });
})

router.get('/get-weather', (req, res) => {
  Weather.find({ 'createdAt': { $gte: new Date(new Date().setHours(00,00,00)) } }, 
    ['cities', 'createdAt']).sort({ 'createdAt': -1 })
  .limit(100).sort({ 'createdAt': 1 }).then((docs, err) => {
    if (err) { 
      return res.json(500, { success: false, error: err });
    }

    return res.json(200, { success: true, data: docs });
  }).catch((err) => {
    if (err) { 
      return res.json(500, { success: false, error: err });
    }
  });
});

module.exports = router;
// /backend/data.js
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// this will be our data base's data structure
const DataSchemaUsers = new Schema(
    {
        googleId: { type: String, required: true },
        isAdmin :  {type: Boolean, default: false },
        firstName: String,
        lastName: String,
        email: String
    },
    { timestamps: true }
);

// export the new Schema so we could modify it using Node.js
module.exports = mongoose.model("User", DataSchemaUsers);

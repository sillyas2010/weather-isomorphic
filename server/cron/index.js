const cron = require('node-cron'),
  config = require('../config'),
  { collectWeather } = require('../func');

module.exports = {
  startCron: () => {
    cron.schedule('*/1 * * * *', () => {
      collectWeather(config.weatherCities);
    })
  }
}
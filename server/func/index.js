const config = require('../config'),
  fetch = require('isomorphic-unfetch'),
  Weather = require('../models/Weather');


module.exports = {
  collectWeather: async (cities) => {
    const citiesQueue = [ ...cities ],
      citiesWeather = [];

    while(citiesQueue.length) {
      const city = citiesQueue.shift(),
        url = config.weatherApiTemplate
          .replace('${apiKey}', config.weatherApiKey)
          .replace('${city}', city),
        cityFetch = await fetch(url),
        cityRes = await cityFetch.json();

      if(cityRes && cityRes.current) {
        citiesWeather.push({
          city,
          temperature: cityRes.current.temperature
        })
      } else {
        citiesWeather.push({
          city,
          temperature: null,
          error: true
        })
      }
    }

    try {
      const weatherObj = await Weather.create({
        cities: citiesWeather
      });

      console.log('INFO: new weather info has arrived');
    } catch (err) {
      console.log(err);
    }
  }
}